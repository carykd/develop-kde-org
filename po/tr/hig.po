#
# Emir SARI <emir_sari@icloud.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-develop-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:19+0000\n"
"PO-Revision-Date: 2023-03-16 16:41+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"SPDX-FileCopyrightText: FULL NAME <EMAIL@ADDRESS>\n"
"SPDX-License-Identifier: LGPL-3.0-or-later\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: content/hig/_index.md:0
msgid "KDE Human Interface Guidelines"
msgstr "KDE İnsan Arayüzü Yönergeleri"

#: content/hig/_index.md:9
msgid ""
"The **KDE Human Interface Guidelines (HIG)** offer designers and developers "
"a set of recommendations for producing beautiful, usable, and consistent "
"user interfaces for convergent desktop and mobile applications and workspace "
"widgets. Our aim is to improve the experience for users by making consistent "
"interfaces for desktop, mobile, and everything in between, more consistent, "
"intuitive and learnable."
msgstr ""
"**KDE İnsan Arayüzü Yönergeleri (HIG)** tasarımcılara ve geliştiricilere "
"yakınsak masaüstü ve taşınabilir uygulamalar ve çalışma alanı pencere "
"ögeleri için güzel, kullanılabilir ve tutarlı kullanıcı arabirimleri "
"üretmeye yönelik bir dizi öneri sunar. Amacımız; masaüstü, taşınabilir ve "
"aradaki her şey için tutarlı, sezgisel ve öğrenilebilir arayüzler "
"oluşturarak kullanıcıların deneyimini iyileştirmektir."

#: content/hig/_index.md:15
msgid "Design Vision"
msgstr "Tasarım Vizyonu"

#: content/hig/_index.md:18
msgid ""
"Our design vision focuses on two attributes of KDE software that connect its "
"future to its history:"
msgstr ""
"Tasarım vizyonumuz, KDE yazılımlarının geleceğini geçmişine bağlayan iki "
"özelliğine odaklanmaktadır:"

#: content/hig/_index.md:21
msgid ""
"![Simple by default, powerful when needed.](/hig/HIGDesignVisionFullBleed."
"png)"
msgstr ""
"![Kendiliğinden yalın, gerektiğinde güçlü.](/hig/HIGDesignVisionFullBleed."
"png)"

#: content/hig/_index.md:23
msgid "Simple by default..."
msgstr "Özünde yalın..."

#: content/hig/_index.md:25
msgid ""
"*Simple and inviting. KDE software is pleasant to experience and easy to use."
"*"
msgstr "*Yalın ve davetkâr. KDE yazılımlarının kullanımı hoş ve kolaydır.*"

#: content/hig/_index.md:28
msgid "**Make it easy to focus on what matters**"
msgstr "**Özünde önemli olan şeylere odaklanmayı yalınlaştır**"

#: content/hig/_index.md:30
msgid ""
"Remove or minimize elements not crucial to the primary or main task. Use "
"spacing to keep things organized. Use color to draw attention. Reveal "
"additional information or optional functions only when needed."
msgstr ""
"Birincil veya ana görev için çok önemli olmayan ögeleri kaldırın veya en aza "
"indirin. İşleri düzenli tutmak için boşluk kullanın. Dikkat çekmek için renk "
"kullanın. Ek bilgiyi veya isteğe bağlı işlevleri yalnızca gerektiğinde "
"gösterin."

#: content/hig/_index.md:35
msgid "**\"I know how to do that!\"**"
msgstr "**\"Bunun nasıl yapılacağını biliyorum!\"**"

#: content/hig/_index.md:37
msgid ""
"Make things easier to learn by reusing design patterns from other "
"applications. Other applications that use good design are a precedent to "
"follow."
msgstr ""
"Diğer uygulamalardaki tasarım kalıplarını yeniden kullanarak öğrenmeyi "
"kolaylaştırın. İyi tasarım kullanan diğer uygulamalar, takip edilmesi "
"gereken bir emsaldir."

#: content/hig/_index.md:41
msgid "**Do the heavy lifting for me**"
msgstr "**Benim için ağır işleri yap**"

#: content/hig/_index.md:43
msgid ""
"Make complex tasks simple. Make novices feel like experts. Create ways in "
"which your users can naturally feel empowered by your software."
msgstr ""
"Karmaşık görevleri basitleştirin. Acemilerin kendilerini uzman gibi "
"hissetmelerini sağlayın. Kullanıcılarınızın yazılımınız tarafından doğal "
"olarak güçlü hissedebilecekleri yollar oluşturun."

#: content/hig/_index.md:47
msgid "...Powerful when needed"
msgstr "...gerektiğinde güçlü"

#: content/hig/_index.md:49
msgid ""
"*Power and flexibility. KDE software allows users to be effortlessly "
"creative and efficiently productive.*"
msgstr ""
"*Güç ve esneklik. KDE yazılımları, kullanıcıların zahmetsizce yaratıcı ve "
"verimli bir şekilde üretken olmalarını sağlar.*"

#: content/hig/_index.md:52
msgid "**Solve a problem**"
msgstr "**Bir sorunu çöz**"

#: content/hig/_index.md:54
msgid ""
"Identify and make very clear to the user what need is addressed and how."
msgstr ""
"Hangi gereksinimin ve nasıl karşılandığının bir tanımını oluşturun ve "
"kullanıcıya açıkça ifade edin."

#: content/hig/_index.md:57
msgid "**Always in control**"
msgstr "**Sürekli denetimde**"

#: content/hig/_index.md:59
msgid ""
"It should always be clear what can be done, what is currently happening, and "
"what has just happened. The user should never feel at the mercy of the tool. "
"Give the user the final say."
msgstr ""
"Neyin yapılabileceği, şu anda neyin olduğu ve tam tanımlaması her zaman açık "
"olmalıdır. Kullanıcı asla aracın insafına kalmış hissetmemelidir. Son sözü "
"kullanıcıya verin."

#: content/hig/_index.md:64
msgid "**Be flexible**"
msgstr "**Esnek ol**"

#: content/hig/_index.md:66
msgid ""
"Provide sensible defaults but consider optional functionality and "
"customization options that don\\'t interfere with the primary task."
msgstr ""
"Makul öntanımlılar sağlayın; ancak birincil göreve müdahale etmeyen isteğe "
"bağlı işlevsellik ve özelleştirme seçeneklerini göz önünde bulundurun."

#: content/hig/_index.md:70
msgid "Note"
msgstr "Not"

#: content/hig/_index.md:72
msgid ""
"KDE encourages developing and designing for customization, while providing "
"good default settings. Integrating into other desktop environments is also a "
"virtue, but ultimately we aim for perfection within our own Plasma desktop "
"environment with the default themes and settings. This aim should not be "
"compromised."
msgstr ""
"KDE, iyi öntanımlı ayarlar sağlarken özelleştirme için geliştirmeyi ve "
"tasarlamayı teşvik eder. Diğer masaüstü ortamları ile tümleşim de bir "
"erdemdir; ancak nihai olarak öntanımlı temalar ve ayarlarla kendi Plasma "
"masaüstü ortamımızda mükemmelliği hedefliyoruz. Bu hedeften taviz "
"verilmemelidir."
